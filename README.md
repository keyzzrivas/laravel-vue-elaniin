# Laravel + Vue Elaniin

This project contains a full implementation of Vue with Laravel, users authentication, password reset and CRUD of product. 

The REST API is using JSON:API specification.

## Demo
You can see the demo [HERE](https://laravel-vue-elaniin-app.herokuapp.com)

Default User:
```bash
User: admin@admin.com
Password: password
```


## Installation

get in stater

```bash
git clone https://gitlab.com/keyzzrivas/laravel-vue-elaniin.git
cd laravel-vue-elaniin
```

run composer

```bash
composer install

cp .env.example .env
php artisan key:generate
php artisan jwt:secret
php artisan storage:link
```
install npm packages & generate files

```
npm install
npm run dev
```


## Data
Run the database migrations and seeders
```
php artisan migration --seed
```

## Usage

```
php artisan serve
```


## Files
[DATABASE](https://gitlab.com/keyzzrivas/laravel-vue-elaniin/-/blob/master/laravel_vue_elaniin.sql)

[POSTMAN COLLETION](https://gitlab.com/keyzzrivas/laravel-vue-elaniin/-/blob/master/larvel_vue_elaniin.postman_collection.json)