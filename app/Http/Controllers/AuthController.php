<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\User;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', [
            'except' => [
                'signIn', 
                'signUp', 
                'session', 
                'forgotPassword',
                'resetPassword'
            ]
        ]);
    }

    /**
     * Permite autenticar al usuario a traves de JWT
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function signIn(Request $request){
        
        $request->validate([
            'email'    => 'required|email',
            'password' => 'required|string|min:6',
            'remember' => 'boolean'
        ]);

        $ttl = ($request->remember === true) ? 1051200 : 1440;
        if (!$token = auth('api')->setTTL($ttl)->attempt($request->except('remember')))
            return response()->json(['error' => 'Invalid email or password'], 401);

        return $this->createNewToken($token);
    }

    /**
     * Registra el usuario
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function signUp(Request $request) {

        $request->validate([
            'name'      => 'required|string|between:3,150',
            'username'  => 'required|string|max:25|unique:users',
            'phone'     => 'required|string|min:6|regex:/[+]([0-9]{3})([ ]?)([0-9]{4})([ -]?)([0-9]{4})/',
            'email'     => 'required|string|email|max:100|unique:users',
            'birthday'  => 'required|date|date_format:Y-m-d|before:yesterday',
            'password'  => 'required|string|confirmed|min:6',
        ]);

        $data = array_merge($request->except('password'), ['password' => bcrypt($request->password)]);
        $user = User::create($data);

        UserResource::withoutWrapping();
        $resource = new UserResource($user);
        return $resource->response()->setStatusCode(201);
    }


    /**
     * Cierra la sesion actual
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function signOut() {
        auth('api')->logout();
        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Actualiza el Token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth('api')->refresh());
    }

    /**
     * Obtiene el usuario en sesion
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function user() {
        
        $user = auth('api')->user();
        UserResource::withoutWrapping();
        return new UserResource($user);
    }

    /**
     * Permite verificar si la sesion esta activa
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function session() {

        $user   = auth('api')->user();
        $status = !empty($user) ? true : false;
        
        $resource = [
            'type' => 'session',
            'attributes' => [
                'active' => $status,
            ]
        ];

        return response()->json($resource);
    }

    /**
     * Permite envair el link de recuperacion de clave de acceso
     */
    public function forgotPassword(Request $request){
        $credentials = $request->validate(['email' => 'required|email']);
        $status      = Password::sendResetLink($credentials);
        return $status === Password::RESET_LINK_SENT ?
                                response()->json(["message" => 'Reset password link sent on your email id.']) :
                                response()->json(["error" => 'Reset password fail or account not found.'], 400);
    }
    
    /**
     * Permite restaurar la clave de acceso actual
     */
    public function resetPassword(Request $request){

        $request->validate([
            'token'    => 'required',
            'email'    => 'required|email',
            'password' => 'required|min:8|confirmed',
        ]);
    
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));
    
                $user->save();
    
                event(new PasswordReset($user));
            }
        );
    
        return $status == Password::PASSWORD_RESET ?
            response()->json(["message" => 'Reset password.']) :
            response()->json(["error" => 'Reset password fail.'], 400);

    }

    /**
     * Devuelve la informacion del token
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function createNewToken($token){
        
        UserResource::withoutWrapping();

        $resource = [
            'type' => 'session',
            'attributes' => [
                'access_token' => $token,
                'token_type'   => 'Bearer',
                'expires_in'   => auth('api')->factory()->getTTL() * 60
            ],
            'included' => [
                new UserResource(auth('api')->user())
            ],
        ];

        return $resource;
    }
}
