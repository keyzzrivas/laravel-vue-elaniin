<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductsResource;
use App\Http\Resources\DeleteProductResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Create a new ProductController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $query = Product::latest();
        $search = $request->search;

        if(!empty($search)){
            $query = $query->where(function($query) use($search) {
                $query->where("name", "LIKE", "%{$search}%")
                    ->orWhere("sku", "LIKE", "%{$search}%");
            });
        }

        return new ProductsResource($query->paginate(9));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // Valida la peticion
        $this->validateRequest($request);
        $product = Product::create($request->except('image'));

        // Almacena la imagen
        $path = "";
        if ($request->hasFile('image')){
            $image = $request->file('image');
            $name  = time().'_product_'.$product->id.'.'.$image->extension();
            $path  = $image->storeAs("products", $name, 'public');
        }

        $product->image = $path;
        $product->save();
        
        ProductResource::withoutWrapping();
        $resource = new ProductResource($product);
        return $resource->response()->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        ProductResource::withoutWrapping();
        return new ProductResource($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        // Valida la peticion
        $this->validateRequest($request);
        $product->fill($request->except('image'));
        $product->save();

        // Almacena la imagen
        $path = $product->image;
        if ($request->hasFile('image')){    
            $image = $request->file('image');
            if(!is_null($image)){
                $name  = time().'_product_'.$product->id.'.'.$image->extension();
                $path  = $image->storeAs("products", $name, 'public');
    
                // Elimina la imagen anterior
                if(Storage::disk('public')->exists($product->image))
                    Storage::disk('public')->delete($product->image);
            }
        }

        $product->image = $path;
        $product->save();
        ProductResource::withoutWrapping();
        return new ProductResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        // Elimina la imagen anterior
        if(Storage::disk('public')->exists($product->image))
            Storage::disk('public')->delete($product->image);

        $response = $product->delete();
        DeleteProductResource::withoutWrapping();
        return new DeleteProductResource($product);
    }

    private function validateRequest(Request $request){

        $this->validate($request, [
            'name'        => 'string|between:3,150',
            'sku'         => 'nullable|string|min:0|max:15',
            'quantity'    => 'required|integer|min:0',
            'image'       => 'nullable|sometimes|image|mimes:png,jpeg,jpg|max:3072',
            'price'       => 'required|numeric|min:0',
            'description' => 'nullable|string',
         ]);
    }
}
