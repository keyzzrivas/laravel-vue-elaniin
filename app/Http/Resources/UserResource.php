<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'users',
            'id'   => (string)$this->id,
            'attributes' => [
                'name'     => $this->name,
                'username' => $this->username,
                'phone'    => $this->phone,
                'email'    => $this->email,
                'birthday' => $this->birthday,
                'email_verified_at' => $this->email_verified_at,
            ],
            'links' => [
                'self' => route('users.show', ['user' => $this->id]),
            ],
        ];
    }
}
