<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $email = 'admin@admin.com';
        $user  = User::where('email', $email)->first();

        if($user) 
            return;

        $user = User::create([
            'name' => 'Admin User',
            'username' => 'admin',
            'phone' => '+503 7000-0000',
            'email' => $email,
            'email_verified_at' => now(),
            'birthday' => now(),
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
        ]);
    }
}
