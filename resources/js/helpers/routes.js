// Obtiene los middlewares
import { middlewares } from './middlewares';

// Obtiene la lista de archivos de la carpeta "components"
const files = require.context('../components', true, /\.vue$/i);
var components =  files.keys().map(key => {

    // Componente
    let component = files(key).default

    // Remueve el simbolo "." en la ruta del archivo
    // Reemplaza el caracter "_" por ":" para el uso de parametros
    // Remueve la extencion del archivo
    let path = key.replace(/^[.]+|[.]+$/g, '').split('/').map(e => e.replace('_', ':')).join('/').split('.')[0];

    // Formatea el nombre de la ruta con guines y lo hace minuscula 
    path = path.split('/').map(e => e.split(/(?=[A-Z])/g).map(s => s.toLowerCase()).join('-').replace(':-', ':')).join('/')

    // Nombre de la ruta
    let name = path.replaceAll(':', '').split('/').filter(e => e).join('-');

    // Remueve la palabra "index" de la ruta
    if(path.endsWith('index'))
        path = path.split('/').slice(0, -1).join('/');

    // Valida la ruta 404
    if(path.endsWith('404'))
        path = "*";

    let item = {
        'name': name,
        'path': (path || "/"), 
        'component': component
    };

    // Asigna los middlewares
    let middlewareNames = component.middleware
    if((middlewareNames || "").trim() == "" || typeof middlewareNames === "undefined")
        return item;

    middlewareNames = Array.isArray(middlewareNames) ? middlewareNames : [middlewareNames]
    let middlewareList = middlewares.filter(e => middlewareNames.indexOf(e.name) !== -1).map(e => e.middlewares);
    
    if(typeof middlewareList  === "undefined" || middlewareList == null)
        return item;

    // Asigna los middlewares
    item.meta = {
        middleware: middlewareList
    };

    return item;
});

/**
 * Exporta las rutas
 */
export const routes = components;

/**
 * Permite obtener los middleware de manera consecutiva
 * @param {*} context 
 * @param {*} middleware 
 * @param {*} index 
 * @returns 
 */
window.nextMiddleware = (context, middleware, index) => {

    const following = middleware[index];

    // Si el proximo middleware no esta definido devuelve el "callback" del contexto
    if (!following)
        return context.next;
  
    return (...parameters) => {
        
        // Ejecuta el callback del router
        context.next(...parameters);

        // Ejecuta el callback del nuevo middleware
        const nextAction = window.nextMiddleware(context, middleware, index + 1);
        following({ ...context, next: nextAction });
    };
}

/**
 * Evento del router para interceptar los middlewares
 * @param {*} to 
 * @param {*} from 
 * @param {*} next 
 * @returns 
 */
export const beforeEach = (to, from, next, router) => {
    
    let middlewares = to.meta.middleware
    if (!middlewares)
        return next();

    // Define el middleware
    const middleware = Array.isArray(middlewares) ? middlewares : [middlewares];
    
    // Define el contexto
    const context = {
        from,
        next,
        router,
        to,
    };

    // Ejecuta el calback del middleware
    const nextMiddleware = window.nextMiddleware(context, middleware, 1);
    return middleware[0]({ ...context, next: nextMiddleware });
};