
export default {
    interceptResponse: function ($vm) {
        axios.interceptors.response.use(function (response) {
            return response;
        }, function (error) {

            if (typeof error.response !== 'undefined' && error.response.status == 401){
                axios.get('/api/v1/auth/session')
                    .then(response => {

                        // valida si el usuario es valido
                        let session = response.data;

                        // Cierra la sesion actual
                        if(session == false || session == 0)
                            $vm.signOut('auth-sign-in')
                    });
            }

            return Promise.reject(error);
        });
    },
    interceptResquest: function($vm){
        axios.interceptors.request.use(function(config) {

                config.headers['Accept'] = 'application/json';
                config.headers['Content-Type'] = 'application/json';

                let token = $vm.$store.get('access_token') || null;
                if (token != null)
                    config.headers["Authorization"] = token;

                return config;
            },
            function(error) {
                return Promise.reject(error);
            }
        );
    }
}