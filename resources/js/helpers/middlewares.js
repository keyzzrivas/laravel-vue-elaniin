// Obtiene la lista de archivos de la carpeta "middlewares"
const files = require.context('../middlewares', true, /\.js$/i);
var modules = files.keys().map(key => {

    let name = key.split('/').pop().split('.')[0]
    return {
        'name': name,
        'middlewares': files(key).default
    };
});

export const middlewares = modules;