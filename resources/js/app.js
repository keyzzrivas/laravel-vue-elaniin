/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

import App from './App.vue';
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueFeather from 'vue-feather';
import { routes, beforeEach } from './helpers/routes';
import Maska from 'maska'
import persistentState from 'vue-persistent-state'
import interceptor from './helpers/interceptor';
import Vue from 'vue';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Asigna los componentes
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(Maska);
Vue.component(VueFeather.name, VueFeather);
Vue.component('pagination', require('laravel-vue-pagination'));

// Set persistence state
Vue.use(persistentState, {
    'access_token' : null,
    'expires_in'   : null,
    'user'         : null
});

// Define el router
const router = new VueRouter({
    mode: 'history',
    routes: routes
});

// Asigna el callvack para interceptar el middleware
router.beforeEach((to, from, next) => {
    return beforeEach(to, from, next, router);
});

// Helpers
Vue.mixin({
    data() {
        return {
            months: [
                "January", 
                "February", 
                "March", 
                "April", 
                "May", 
                "June",
                "July", 
                "August", 
                "September", 
                "October", 
                "November", 
                "December"
            ]
        }
    },
    methods: {
        signOutRequest: function(redirect = null){
            var $vm = this;
            axios.post('/api/v1/auth/signout')
            .then(response => {
                $vm.signOut(redirect);
            }).catch(error => {
                $vm.signOut(redirect);
            });
        },
        signOut: function (redirect = null) {
            
            // Limpia el almacenamiento
            this.$store.clearAll()

            // Reasigna las variable para actualizar el estado
            this.user = null;
            this.access_token = null;
            this.expires_in = null;

            // Eliminar el header de autorizacion
            window.axios.defaults.headers.common['Authorization'] = null;

            // Redirecciona al login
            if(redirect !== null)
                this.$router.push({ name: redirect });
        }
    }
})

// La instancia principal de vue
const app = new Vue({
    el: '#app',
    router: router,
    render: h => h(App)
});

// Agrega los interceptore de axios
interceptor.interceptResquest(app);
interceptor.interceptResponse(app);

window.$vmContext = app;