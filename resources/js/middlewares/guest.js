export default function auth({ next, router }) {
    
    let token = router.app.$store.get('access_token') || null;
    if (token != null)
        return router.push({ name: 'index' });
  
    return next();
}