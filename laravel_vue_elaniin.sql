-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para laravel_vue_elaniin
DROP DATABASE IF EXISTS `laravel_vue_elaniin`;
CREATE DATABASE IF NOT EXISTS `laravel_vue_elaniin` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `laravel_vue_elaniin`;

-- Volcando estructura para tabla laravel_vue_elaniin.failed_jobs
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla laravel_vue_elaniin.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla laravel_vue_elaniin.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla laravel_vue_elaniin.migrations: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(9, '2014_10_12_000000_create_users_table', 1),
	(10, '2014_10_12_100000_create_password_resets_table', 1),
	(11, '2019_08_19_000000_create_failed_jobs_table', 1),
	(12, '2021_05_08_220532_create_products_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla laravel_vue_elaniin.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla laravel_vue_elaniin.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla laravel_vue_elaniin.products
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla laravel_vue_elaniin.products: ~50 rows (aproximadamente)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `sku`, `quantity`, `image`, `price`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'Et sit consequatur commodi.', '2999757865018', 69, 'https://images.unsplash.com/photo-1585692352038-83025e0333bf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=334&q=80', 80.05, 'Nobis non quis similique id eum. Delectus molestias necessitatibus architecto illum. Aut porro distinctio commodi quia.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(2, 'Aut fugit.', '9289242126469', 86, 'https://images.unsplash.com/photo-1614179818511-4bb0af32e44a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=375&q=80', 52.15, 'Blanditiis velit rem vel dolorem totam. Cumque excepturi inventore laborum minima. Qui et accusamus sed mollitia et corrupti voluptate.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(3, 'Aliquid ad aliquid sint.', '1391033628163', 5, 'https://images.unsplash.com/photo-1585692352038-83025e0333bf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=334&q=80', 243.22, 'Nulla et voluptatem deserunt quo voluptates. Nesciunt et voluptatibus sed. Voluptatem officiis nulla nemo in vel aut.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(4, 'Repellendus nesciunt consequuntur.', '1671187236491', 83, 'https://images.unsplash.com/photo-1590658058105-af4b65f8871b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=889&q=80', 20.94, 'Odio omnis natus rerum perferendis. Vitae deleniti excepturi quia quae quidem minima explicabo. Sed cumque recusandae vero et molestiae aspernatur vel. Ut exercitationem consequatur qui quisquam quidem qui enim.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(5, 'Et ab esse.', '2094083785266', 18, 'https://images.unsplash.com/photo-1618366712010-f4ae9c647dcb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=334&q=80', 15.84, 'Reprehenderit aut sit repellendus inventore unde. Doloribus eos ad quia consequatur aut. Similique aut tempore quaerat cupiditate delectus alias.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(6, 'Ratione natus ut sed.', '3847342468122', 7, 'https://images.unsplash.com/photo-1497382706140-605ee76b3b55?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=750&q=80', 33.49, 'Expedita enim id asperiores quisquam cum. Vero velit et temporibus. Dolores deserunt ut voluptas. Expedita dolores consequatur quaerat iste optio explicabo recusandae. Id omnis velit autem inventore nihil ducimus alias nihil.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(7, 'Saepe eius earum libero.', '5661305401289', 28, 'https://images.unsplash.com/photo-1618057411063-c2509426adf6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=660&q=80', 111.74, 'Quam tenetur iure et qui voluptatem voluptatem. Fuga et ut fugit eveniet illo. Velit et et aut voluptas laboriosam.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(8, 'Quibusdam non voluptate.', '0670520936778', 37, 'https://images.unsplash.com/photo-1554200876-907f9286c2a1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=750&q=80', 429.80, 'Aut labore alias ad dicta iusto quisquam. Officiis autem ipsam dolorem numquam. Eius quos sed nesciunt cumque.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(9, 'Ut qui consequatur.', '2542079563683', 12, 'https://images.unsplash.com/photo-1497382706140-605ee76b3b55?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=750&q=80', 383.92, 'Quam sit ut nostrum quia. Minus aut quam repellat voluptas. Dignissimos voluptatem natus quo est. Quis blanditiis omnis impedit consequatur minima minima.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(10, 'Quia assumenda exercitationem qui architecto.', '8237738856988', 1, 'https://images.unsplash.com/photo-1614755192999-b8ecede15458?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=80', 415.93, 'Repellendus fugit quia dolore exercitationem aut repudiandae voluptates eius. Numquam quam deleniti error fugiat. Praesentium distinctio neque recusandae vel culpa ea necessitatibus. Neque et commodi qui soluta culpa. Et quod quas in laboriosam eligendi.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(11, 'Id ut vitae.', '9667209792047', 78, 'https://images.unsplash.com/photo-1590658058105-af4b65f8871b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=889&q=80', 400.04, 'Vitae aut est ut sint assumenda. Placeat id maiores earum rerum maxime quisquam perferendis. Possimus et voluptatem commodi qui molestiae.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(12, 'Sed rerum sunt.', '1979210528088', 89, 'https://images.unsplash.com/photo-1618057411063-c2509426adf6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=660&q=80', 71.09, 'Voluptate excepturi pariatur nesciunt fuga at omnis. Aut atque voluptas ipsam eligendi provident quas est aliquam. Dolores quo aut iste facilis id. Rerum velit aut asperiores error.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(13, 'Iure debitis neque placeat.', '4631585955766', 11, 'https://images.unsplash.com/photo-1617781007952-d4c73105a022?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80', 330.36, 'Voluptas sequi est consequatur in. Quas maxime ipsum sunt possimus fugit. Maiores rerum accusamus repudiandae.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(14, 'Omnis doloribus est quam.', '9341493534889', 2, 'https://images.unsplash.com/photo-1618366712010-f4ae9c647dcb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=334&q=80', 492.96, 'Neque officiis officia sit sapiente natus sed illo reiciendis. Tempore id aliquid nobis quos repellat molestias magni. Ut a repellat et sed magni. Aut enim excepturi fugit alias quas esse in qui. Sapiente atque commodi minus repudiandae nihil non.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(15, 'Eius impedit eius in omnis.', '8414114991819', 41, 'https://images.unsplash.com/photo-1580707246035-ae392a27bb2c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80', 444.82, 'In exercitationem aperiam aut nemo fugiat voluptatem et vero. Sed eius modi error mollitia laboriosam. Praesentium ea sunt sint qui aut rerum porro. Voluptatem aut totam quisquam earum fugiat.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(16, 'Dolor fugit saepe facere.', '9549870751885', 58, 'https://images.unsplash.com/photo-1619892203894-b8c313da1af2?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80', 258.40, 'Impedit ut harum quia sunt qui qui. Modi nobis enim quisquam quasi eaque. Qui perspiciatis voluptatem delectus perferendis quisquam fugiat repellendus. Et dicta et similique.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(17, 'Sunt itaque perferendis.', '3631922308881', 15, 'https://images.unsplash.com/photo-1508685096489-7aacd43bd3b1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1482&q=80', 481.57, 'Non non dolor officia cupiditate ut in. Est reiciendis expedita omnis quam nobis. Cumque sed qui possimus sapiente deleniti eius omnis magnam. Ea adipisci ea odit dolore vitae at dolor.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(18, 'Impedit omnis nesciunt rerum explicabo.', '5291489308816', 17, 'https://images.unsplash.com/photo-1617109224926-b69d0862ef1b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80', 107.32, 'A dolore ad praesentium. Earum quis quae quia. Voluptatem ratione voluptatem facere aperiam. Commodi aut est exercitationem molestias beatae eveniet quia.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(19, 'Eaque ullam.', '7828752887938', 86, 'https://images.unsplash.com/photo-1614179818511-4bb0af32e44a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=375&q=80', 19.47, 'Eos nemo et molestias autem ratione et. Aut quo quasi occaecati quidem non laboriosam esse. Perspiciatis dignissimos natus voluptatibus quasi vitae.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(20, 'Est et laborum animi quos.', '4314205940945', 33, 'https://images.unsplash.com/photo-1618366712010-f4ae9c647dcb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=334&q=80', 194.67, 'Sunt voluptas aliquam qui quis consequatur placeat sapiente dolorum. Hic eum ut minima et assumenda. Exercitationem quaerat qui aliquid voluptas vitae. Voluptatum est ut qui sed doloribus et.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(21, 'Nulla quam tempore.', '9911164870525', 17, 'https://images.unsplash.com/photo-1614179818511-4bb0af32e44a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=375&q=80', 307.74, 'Cum corrupti ut quia assumenda assumenda. Deserunt laudantium dolores cumque voluptatibus voluptatem qui. Dicta at architecto voluptatem tempore. Dolor iure iure corrupti molestias dolore et.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(22, 'Eum eligendi eligendi accusantium.', '8821870175672', 92, 'https://images.unsplash.com/photo-1616341317041-cf93b2389ef5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80', 274.28, 'Blanditiis fugit cumque voluptatem voluptates deserunt provident. Qui cupiditate quia maiores. Magnam nostrum ad voluptatem optio. Ratione ut similique quas est numquam dolor veniam. Inventore rerum aut reiciendis quis numquam quis.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(23, 'Rerum qui rerum rerum est.', '5705615726554', 42, 'https://images.unsplash.com/photo-1591105575839-1fb30d5ce4a5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80', 417.41, 'Consequuntur quia fugit omnis ex. Quasi iste in ipsa sit. Totam animi et magnam non veritatis expedita dolore. Ipsam omnis impedit dignissimos in doloribus et.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(24, 'Ullam non earum reiciendis.', '7223701600673', 4, 'https://images.unsplash.com/photo-1579829366248-204fe8413f31?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=750&q=80', 312.87, 'Dolores soluta perferendis voluptates. Ipsam odit voluptatem voluptas doloribus minus. Magnam vero qui alias expedita. Ad ipsam omnis porro doloribus placeat ab.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(25, 'Nihil asperiores sequi voluptatem voluptas.', '5795416741610', 84, 'https://images.unsplash.com/photo-1614179818511-4bb0af32e44a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=375&q=80', 161.93, 'Magni molestiae omnis ea laborum. Autem inventore unde vel suscipit.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(26, 'Modi voluptatum sit.', '6032808346589', 66, 'https://images.unsplash.com/photo-1607930934636-279238005b0e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80', 419.24, 'Ipsum quidem laboriosam consequatur tempore. Qui sed debitis aut sit. Aliquid voluptatem quo ut nihil. Consectetur consequatur impedit enim minima quia.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(27, 'Tenetur repellendus et voluptates.', '6521965351661', 77, 'https://images.unsplash.com/photo-1504282101952-7fb308d9ddf8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=750&q=80', 1.67, 'Sapiente debitis eos at porro neque in excepturi. In quibusdam soluta nobis aut illo quod. Non facere vel soluta voluptatem nihil ratione.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(28, 'Nisi necessitatibus rem.', '2234883886408', 57, 'https://images.unsplash.com/photo-1618366712010-f4ae9c647dcb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=334&q=80', 362.85, 'Culpa ut et id illum et sunt ratione. Et cum at ipsa maxime eum. Eaque vel in officia sunt odio qui.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(29, 'Voluptatibus est eaque explicabo.', '8967657313469', 20, 'https://images.unsplash.com/photo-1607930934636-279238005b0e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80', 119.06, 'Nesciunt dolore aut sint. Ut debitis cumque est dolores. Est eos ducimus sed unde temporibus impedit nesciunt.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(30, 'Quidem voluptatibus sequi.', '7838233934266', 42, 'https://images.unsplash.com/photo-1590658058105-af4b65f8871b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=889&q=80', 351.17, 'Possimus quibusdam ducimus eos fuga cum odio. Numquam harum dicta et perspiciatis. Nam est molestias omnis possimus.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(31, 'Neque et accusamus voluptatem.', '4374590683155', 87, 'https://images.unsplash.com/photo-1591105575839-1fb30d5ce4a5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80', 61.47, 'Amet enim maiores cum. Nemo eius ex culpa delectus et. Repudiandae autem sint quibusdam voluptas autem hic. Harum temporibus ullam exercitationem animi numquam et fugit.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(32, 'Sint nostrum perferendis.', '2042815208842', 22, 'https://images.unsplash.com/photo-1585692352038-83025e0333bf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=334&q=80', 98.68, 'Ab dolorem omnis amet. Ut et cupiditate animi et provident sapiente. Eveniet molestiae at et consequuntur vitae dolorum. Voluptas asperiores ipsam tenetur nostrum occaecati.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(33, 'Quasi laboriosam provident voluptatibus.', '5315483733761', 22, 'https://images.unsplash.com/photo-1580707246035-ae392a27bb2c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80', 367.01, 'Omnis nam consequatur ducimus omnis. Non ut officia distinctio id placeat ut ut. Vitae provident ratione harum dolorem quod.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(34, 'Id ut fugit consequuntur.', '8625801993171', 22, 'https://images.unsplash.com/photo-1508685096489-7aacd43bd3b1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1482&q=80', 63.67, 'Quidem iure aut doloribus enim dolor cumque. Sunt nobis veniam est quas minus neque sint. Sunt soluta molestiae quae. Maxime tempora saepe soluta ut labore quia.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(35, 'Sit nobis et.', '8088350278142', 71, 'https://images.unsplash.com/photo-1616341317041-cf93b2389ef5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80', 161.13, 'Facilis at quis ducimus dolorum suscipit repellendus totam. Numquam laborum non laudantium praesentium id totam laboriosam. Officia dolorum illo cum. Expedita quis ea nobis voluptatibus ut doloremque possimus aut.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(36, 'Adipisci maiores officia porro.', '3395201227651', 61, 'https://images.unsplash.com/photo-1508685096489-7aacd43bd3b1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1482&q=80', 303.48, 'Vel ut ad ut optio cumque corporis. Ut voluptatibus cum tempora temporibus distinctio. Voluptatem modi occaecati ut. Nihil reiciendis nostrum cupiditate vel illo et.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(37, 'Et sed ut dicta.', '5081281583197', 44, 'https://images.unsplash.com/photo-1554200876-907f9286c2a1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=750&q=80', 307.52, 'Hic labore et est rerum. Earum sed ducimus assumenda occaecati officiis omnis eos. Saepe saepe aut impedit ratione eum aliquam eum. Asperiores iure sed aut fuga vitae ex.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(38, 'Ea nostrum maxime non.', '3107414212504', 54, 'https://images.unsplash.com/photo-1618057411063-c2509426adf6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=660&q=80', 383.24, 'Neque iure incidunt odio id. Aut dolor animi corporis exercitationem commodi reiciendis animi. Eum ut quidem fugiat voluptatum mollitia voluptatum commodi. Praesentium dolorum blanditiis repudiandae assumenda non minima quos.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(39, 'Natus sed laborum.', '9851461285182', 94, 'https://images.unsplash.com/photo-1619892203894-b8c313da1af2?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80', 143.74, 'Ut enim laborum rem iure est ipsa. Culpa dicta cumque ullam quo nihil et. Velit aliquid in nam et occaecati numquam. Iste eligendi mollitia aut.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(40, 'Magnam inventore autem rerum repellendus.', '2334711949333', 41, 'https://images.unsplash.com/photo-1617109224926-b69d0862ef1b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80', 399.06, 'Amet optio voluptas qui sed. Voluptatem enim quas et aperiam qui quo aspernatur cum. Doloribus vitae voluptatem voluptatem. Et id sed quis qui unde consequatur modi.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(41, 'Fugiat excepturi amet.', '0275474694332', 17, 'https://images.unsplash.com/photo-1504282101952-7fb308d9ddf8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=750&q=80', 208.11, 'Enim perferendis vero saepe. Doloremque eum quaerat odio quo in. Est pariatur ad suscipit sint error illum. Eum dolores esse eum nobis.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(42, 'Quae est sit.', '5207909958639', 23, 'https://images.unsplash.com/photo-1579829366248-204fe8413f31?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=750&q=80', 379.12, 'Fugit dolores accusamus explicabo accusamus deleniti nam. Tempore aut incidunt incidunt quidem nam. Laudantium est dolores quae consectetur. Modi occaecati officia distinctio quas sint dolor ut.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(43, 'Dicta maxime nihil.', '4630408551833', 25, 'https://images.unsplash.com/photo-1508685096489-7aacd43bd3b1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1482&q=80', 403.45, 'Expedita omnis corrupti neque eos ut ut ratione. Aut saepe excepturi nam reprehenderit sed illo. Saepe quidem voluptates praesentium ad error. Est suscipit temporibus voluptatum fugiat.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(44, 'Voluptates corrupti impedit molestiae.', '9401011248831', 22, 'https://images.unsplash.com/photo-1580707246035-ae392a27bb2c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80', 444.14, 'Expedita omnis voluptas quod tempora modi hic suscipit fugit. Quis perferendis et quasi perspiciatis dolorem autem. Voluptatem corrupti eos sunt. Culpa quis ipsam et eius hic.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(45, 'Et tempora unde.', '7942767984243', 48, 'https://images.unsplash.com/photo-1585692352038-83025e0333bf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=334&q=80', 228.26, 'Qui quia porro voluptatem inventore et. Corporis consequatur asperiores pariatur quidem consequatur molestiae.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(46, 'Voluptates alias consectetur quo.', '4525220357742', 4, 'https://images.unsplash.com/photo-1504282101952-7fb308d9ddf8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=750&q=80', 259.37, 'Quos provident autem vel earum necessitatibus quidem porro quo. Voluptas harum ut excepturi qui minima mollitia. Dolorum est quam illum praesentium.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(47, 'Nostrum ratione aut.', '9263717248368', 0, 'https://images.unsplash.com/photo-1618057411063-c2509426adf6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=660&q=80', 86.31, 'Omnis eaque neque asperiores unde voluptas a laudantium. Nesciunt possimus quam rerum sint eveniet repellendus officia. Voluptatum cupiditate eveniet dolor est. Sint maiores corrupti numquam.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(48, 'Ad nihil.', '9841115140196', 64, 'https://images.unsplash.com/photo-1591105575839-1fb30d5ce4a5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80', 405.99, 'Officia error tenetur nisi qui magni suscipit. Aut minima qui et perspiciatis. Quo est doloribus id incidunt nostrum aut.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(49, 'Ipsam ad nam.', '0929505990206', 33, 'https://images.unsplash.com/photo-1508685096489-7aacd43bd3b1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1482&q=80', 67.44, 'Eum eius facere autem nulla. In dolores cumque ducimus. Assumenda architecto exercitationem nam tempora quia voluptatem rem.', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(50, 'Labore assumenda quia id quasi.', '8845326508753', 100, 'https://images.unsplash.com/photo-1617781007952-d4c73105a022?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80', 304.00, 'Dignissimos porro optio at labore quidem. Est numquam eius vel laboriosam et. Labore iste et reiciendis voluptatem blanditiis. Reiciendis accusamus maxime ut dolore quidem est dolor.', '2021-05-11 06:16:47', '2021-05-11 06:16:47');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Volcando estructura para tabla laravel_vue_elaniin.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `birthday` date NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla laravel_vue_elaniin.users: ~51 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `username`, `phone`, `email`, `email_verified_at`, `birthday`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Admin User', 'admin', '+503 7000-0000', 'admin@admin.com', '2021-05-11 06:16:46', '2021-05-11', '$2y$10$rhbjqVpOfsjrHXBk/r0vFOtyEWKGuro87G6Q4XmMFChOzioz6FJ7S', 'MM3TEbulIx', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(2, 'Mafalda Zulauf II', 'tleuschke', '+503 7000-0000', 'reta.labadie@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7tKzs0FNQi', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(3, 'Bernard Emmerich', 'tleffler', '+503 7000-0000', 'brakus.ahmed@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dO5U64QtNq', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(4, 'Stephan Auer IV', 'anjali17', '+503 7000-0000', 'samara.heller@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Dz1d80dhEV', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(5, 'Jerel Green', 'vweimann', '+503 7000-0000', 'terence.shields@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'LuYN36e0EH', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(6, 'Brian Carter', 'casandra.paucek', '+503 7000-0000', 'summer.kuphal@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'N7ViVxySmP', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(7, 'Florine Morissette', 'stamm.joseph', '+503 7000-0000', 'torey37@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'tT4nOBUejO', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(8, 'Miss Dana Herman Jr.', 'hoppe.magnolia', '+503 7000-0000', 'ehaag@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '4M0Tm8TqKW', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(9, 'Odell Ratke', 'tevin.kulas', '+503 7000-0000', 'wlehner@example.org', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '4ingBtNdvq', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(10, 'Hyman Barton MD', 'jenkins.kathlyn', '+503 7000-0000', 'jamaal.mclaughlin@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Qw0nA0r7fu', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(11, 'Robert Schiller', 'itreutel', '+503 7000-0000', 'rohan.alvera@example.org', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'sjwm3lJILr', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(12, 'Branson Ebert', 'tromp.efrain', '+503 7000-0000', 'anna.connelly@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'evFdy0bmQc', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(13, 'Dale Gaylord', 'emmanuel73', '+503 7000-0000', 'kelli.watsica@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ShVTe28Apr', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(14, 'Nedra Luettgen DDS', 'brekke.nichole', '+503 7000-0000', 'hamill.amya@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'VhKEdqWD08', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(15, 'Miss Christelle Roob Jr.', 'lind.dejuan', '+503 7000-0000', 'lesley.mclaughlin@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'cfn2ME79CW', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(16, 'Eriberto Pagac', 'josefa.schoen', '+503 7000-0000', 'qmurray@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7eo3Ub8pTR', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(17, 'Alta Waters', 'skye.rogahn', '+503 7000-0000', 'tabitha88@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'WjsNKAqvnG', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(18, 'Reece Rutherford', 'blaise71', '+503 7000-0000', 'delaney24@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'cxQwKXouoE', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(19, 'Mariane Borer MD', 'lola22', '+503 7000-0000', 'ebergstrom@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dgu5YADBRa', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(20, 'Mr. Niko Koss DDS', 'xschuppe', '+503 7000-0000', 'lorna.bogan@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'sTMnxWp9Nc', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(21, 'Gerhard Stanton', 'angela.gaylord', '+503 7000-0000', 'tatum.ernser@example.org', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'FZ8J97tNjh', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(22, 'Stefan Bailey DVM', 'lrunolfsson', '+503 7000-0000', 'cassandra25@example.org', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'JH5nbt3xhZ', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(23, 'Prof. Eliane Heathcote V', 'tparisian', '+503 7000-0000', 'sauer.deion@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'v8MPP0LGV5', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(24, 'Pierce Maggio DVM', 'maybelle.schuppe', '+503 7000-0000', 'ernser.evie@example.org', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'kdUsQ4MdjH', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(25, 'Fritz Johns', 'geraldine74', '+503 7000-0000', 'ofeeney@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'sF54vdjb27', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(26, 'Dr. Shakira Haley MD', 'mmills', '+503 7000-0000', 'yschultz@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'HwN24IJZx7', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(27, 'Roel Bruen', 'sam.robel', '+503 7000-0000', 'felipe04@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Ld2QNQwnj1', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(28, 'Mara Jaskolski III', 'ferry.oscar', '+503 7000-0000', 'adonis96@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YdjrX3rrMB', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(29, 'Prof. Gay Harber V', 'rwalter', '+503 7000-0000', 'green.michaela@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'HewrUV2U73', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(30, 'Kieran Dietrich', 'kailey.bosco', '+503 7000-0000', 'mbayer@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '1CDXxkD5Nm', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(31, 'Karley Kihn', 'mclaughlin.robin', '+503 7000-0000', 'odessa.lind@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '1BVo7dYs8b', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(32, 'Sage Casper', 'pwest', '+503 7000-0000', 'obotsford@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Nyi88Qvohs', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(33, 'Reuben Nikolaus', 'heather92', '+503 7000-0000', 'amie75@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'n3zLmW70Ad', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(34, 'Mr. Darren Kuhn III', 'nickolas98', '+503 7000-0000', 'richmond.hintz@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'VRi2WBow4j', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(35, 'Prof. Jose Gutkowski MD', 'jpadberg', '+503 7000-0000', 'newton83@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'djA0mhAQLF', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(36, 'Aimee Senger', 'kbashirian', '+503 7000-0000', 'guiseppe67@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Z4WZuBYzgx', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(37, 'Mrs. Tessie Nolan', 'mschamberger', '+503 7000-0000', 'sage88@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'f0685dfTXZ', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(38, 'Jevon Schiller', 'marlin.rohan', '+503 7000-0000', 'dillon.reichert@example.org', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2RnEaKLTCE', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(39, 'Miss Lucienne Fritsch', 'adele40', '+503 7000-0000', 'croberts@example.org', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'zPkFHnqtNv', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(40, 'Hal Kiehn MD', 'abbott.pascale', '+503 7000-0000', 'hickle.ophelia@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'RRWTD0CKrO', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(41, 'Elias Harber', 'towne.lia', '+503 7000-0000', 'roger95@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'zl5qub20q0', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(42, 'Mrs. Letha Cartwright MD', 'gleason.efren', '+503 7000-0000', 'rgleichner@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'nWLsSapJb6', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(43, 'Clotilde Homenick Jr.', 'misael88', '+503 7000-0000', 'myles.kuhic@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'n4qpdo9Q2Q', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(44, 'Luis Paucek', 'keely37', '+503 7000-0000', 'hamill.elias@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'W86Hwy7Zd2', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(45, 'Ally Funk', 'joany47', '+503 7000-0000', 'ibergnaum@example.com', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dvYQKd3iG2', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(46, 'Jamir Koss', 'santos10', '+503 7000-0000', 'jackeline35@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'MyTgKg12qi', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(47, 'Joanne Satterfield', 'uriel.veum', '+503 7000-0000', 'damon77@example.org', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'AWQ3Fm4oFg', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(48, 'Amiya Mante', 'xhauck', '+503 7000-0000', 'dasia.ruecker@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'HhRCx70oQz', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(49, 'Eliza Hane III', 'doyle.torrey', '+503 7000-0000', 'abe00@example.org', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'znQFCcEb8Y', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(50, 'Dashawn Rippin', 'jacinthe.king', '+503 7000-0000', 'damian.gleason@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'iCZSjaOhl5', '2021-05-11 06:16:47', '2021-05-11 06:16:47'),
	(51, 'Jerel Williamson', 'bernier.libbie', '+503 7000-0000', 'halle70@example.net', '2021-05-11 06:16:47', '2021-05-11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'aftRPSEPUy', '2021-05-11 06:16:47', '2021-05-11 06:16:47');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
