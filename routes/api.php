<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Controladores
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function ($router) {

    Route::group([
        'prefix' => 'auth'
    ], function () {
        Route::post('/signin',  [AuthController::class, 'signIn']);
        Route::post('/signup',  [AuthController::class, 'signUp']);
        Route::post('/signout', [AuthController::class, 'signOut']);
        Route::post('/refresh', [AuthController::class, 'refresh']);
        Route::get('/user',     [AuthController::class, 'user']);
        Route::get('/session',  [AuthController::class, 'session']);

        Route::post('/forgot-password', [AuthController::class, 'forgotPassword']);
        Route::post('/reset-password', [AuthController::class, 'resetPassword']);
    });

    Route::apiResource('products', ProductController::class)->except([
        'create',
        'edit'
    ]);

    Route::apiResource('users', UserController::class)->except([
        'create',
        'edit',
        'store'
    ]);
});